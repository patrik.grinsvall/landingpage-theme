var path = require('path');

module.exports = {
    context: path.resolve(__dirname, 'assets'),

    entry: {
        main: ['./js/main.js'],
    },

    output: {
        path: path.resolve(__dirname, './dist/js/'),
        filename: '[name].js'
    },

    optimization: {
        splitChunks: {
            'chunks': 'all',
            maxInitialRequests: Infinity,
            cacheGroups: {
                commons: {
                    test: /[\\/]node_modules[\\/]/,
                    name(module) {
                        const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
                        return packageName;
                    }
                }
            }
        }
    },
    
    // optimization: {
    //     runtimeChunk: 'single',
    //     splitChunks: {
    //       chunks: 'all',
    //       maxInitialRequests: Infinity,
    //       minSize: 0,
    //       cacheGroups: {
    //         vendor: {
    //           test: /[\\/]node_modules[\\/]/,
    //           name(module) {
    //             // get the name. E.g. node_modules/packageName/not/this/part.js
    //             // or node_modules/packageName
    //             const packageName = module.context.match(/[\\/]node_modules[\\/](.*?)([\\/]|$)/)[1];
    
    //             // npm package names are URL-safe, but some servers don't like @ symbols
    //             return `npm.${packageName.replace('@', '')}`;
    //           },
    //         },
    //       },
    //     },
    //   },
    module: {
        rules: [{
                test: /\.js$/,
                exclude: /(node_modules|bower_components)/,
                use: {
                    loader: 'babel-loader',
                    options: {
                        presets: ['@babel/preset-env']
                    }
                }
            }
        ]
    },
}