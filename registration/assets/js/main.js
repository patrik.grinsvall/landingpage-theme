import $ from 'jquery';
import 'bootstrap';
import datepicker from 'bootstrap-datepicker';


//check if terms is checked
$('#terms').on('change', function (e) {
    var registerBtn = $('[type="submit"]');
    if (registerBtn.attr('disabled')) {
        registerBtn.removeAttr('disabled');
    } else {
        registerBtn.attr('disabled', true);
    }
})

$(document).on('ready', function () {
    //init datepicker
    $('.birth').datepicker();
})