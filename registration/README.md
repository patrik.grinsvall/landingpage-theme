Registration page

## Installation
-  Copy the entire folder registration to your server running php / mysql.
-  Access the registration page with a webbrowser,  "https://<yourservername.com>/registration" if you placed the registration directory in the web root of your site. Otherwise change the path accordingly.

## Database
-  If you already have a database, change the name of the database and the username/password combination in the top of the index.php file:
```
    $db = mysqli_connect(
        'localhost',        // database hostname
        'root',             // database username
        '',                 // database password
        'registration');    // database name table.
```
-  This code assumes your users are stored in a 'users' table, if the table name is different change it on line 50: 
```
            $sql = sprintf("INSERT INTO users".
``` 
-  You probably need to change the mapping of the input fields to the database columns, at line 52: 
```
                    // change here to your database column name for firstname 
                    "firstname,".
                    // change here to your database column name for lastname
                    "lastname,".
                    // change here to your database column name for email
                    "email,".
                    // change here to your database column name for birthdate
                    "birth,".
                    // change here to your database column name for gender
                    "gender,".
                    // change here to your database column name for phonenumber
                    "phone_number,".
                    // change here to your database column name for nationality
                    "nationality) ".
```
## If you dont have a database
-  A database structure is attached, `users.sql`. Import into mysql with the command: `mysql -hyoursite.com -uyourusername -pyourpassword youdatabasename < users.sql`
