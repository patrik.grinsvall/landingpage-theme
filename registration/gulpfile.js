var gulp = require('gulp'),
    webpack = require('webpack'),
    gulp_sass = require('gulp-sass'),
    sourcemaps = require('gulp-sourcemaps'),
    autoprefixer = require('gulp-autoprefixer'),
    imagemin = require('gulp-imagemin'),
    browserSync = require('browser-sync').create();


var css = './assets/css/**/*.css',
    js = './assets/js/**/*.js',
    sass = './assets/sass/**/*.scss',
    img = './assets/img/**/*',
    fonts = './assets/fonts/*',
    svgs = './assets/svgs/**/*';

function SASS() {
    return gulp.src(sass)
        .pipe(sourcemaps.init())
        .pipe(gulp_sass())
        .on('error', function (err) {
            console.log(err.toString());
            this.emit('end');
        })
        .pipe(autoprefixer('last 75 versions'))
        .pipe(sourcemaps.write())
        .pipe(gulp.dest('dist/css/'))
        .pipe(browserSync.stream())
}


function imgMin() {
    return gulp.src(img)
        .pipe(imagemin())
        .pipe(gulp.dest('./dist/images'))
        .pipe(browserSync.stream())
}

function copyFont() {
    return gulp.src(fonts)
        .pipe(gulp.dest('dist/fonts/'))
}

function copySVG() {
    return gulp.src( svgs )
        .pipe(gulp.dest('dist/svgs'))
}

function copyCSS(){
    return gulp.src( css )
        .pipe(gulp.dest('dist/css'))
}

function compileJS() {
    return gulp.src ( 'dist/js' )
        .pipe(browserSync.stream())
}


function watch() {

    gulp.watch(sass, SASS);
    gulp.watch(js, gulp.series(webpackScript, compileJS));
    gulp.watch(img, imgMin);
    gulp.watch(fonts, copyFont);
    gulp.watch(svgs, copySVG);
    gulp.watch(css, copyCSS);
    gulp.watch('**/*.php').on('change', browserSync.reload);
}


function webpackScript(callback) {
    webpack(require('./webpack.config.js'), function (err, stats) {
        if(err){
            console.log(err.toString())
        }else{
            console.log(stats.toString())
        }
        callback();
    })
}


gulp.task('watch', watch);
gulp.task('webpackScript', webpackScript);
gulp.task('default', gulp.series(copyFont, SASS, copyCSS, copySVG, imgMin,watch));
